import numpy as np
import cv2


def imwrite(im, name):
    cv2.imwrite(name + '.jpg', cv2.resize(im * 255, None, fx=20, fy=20,
                                          interpolation=cv2.INTER_AREA))


def show(im):
    cv2.imshow('', im)
    k = cv2.waitKey(30) & 0xff

def main():
    # stawberries()
    building()


def zero_crossings(img):
    im = np.sign(img)
    for i in range(1, im.shape[0]):
        for j in range(1, im.shape[1]):
            if im[i, j] != im[i-1][j] or im[i, j] != im[i][j-1]:
                yield (i, j)


def building():
    im = cv2.imread("building.tiff", cv2.IMREAD_GRAYSCALE) / 255
    max = np.min(im.shape)
    print(max)
    for k in range(3, max, 2):
        im = cv2.GaussianBlur(im, (k, k), 1)
        # show(im)
        cv2.imshow('image', im)

        lap = cv2.Laplacian((im*255).astype(np.uint8), cv2.CV_16S)
        lap_clip = np.clip(lap + 0.5, 0, 1)
        # show(lap_clip)
        cv2.imshow('lap_clip', lap_clip)

        im_bgr = np.dstack([lap_clip]*3)
        im_grey = im_bgr.copy()

        for i, j in zero_crossings(lap):
            im_bgr[i, j] = (0, 0, 255)

        # show(im_bgr)
        cv2.imshow('im_red', im_bgr)

        if ((k-3)/2)%50 == 0 or k==3:
            print("saving, ", k)
            combo = np.hstack((np.dstack([im*255]*3), im_grey*255, im_bgr))
            cv2.imwrite('combo_'+str(k)+".jpg", combo)
        if (k-3)/2 % 10 == 0:
            print("k = ", k)
        cv2.waitKey(5)



def stawberries():
    im_pattern = cv2.imread("strawberries-binary.pbm", cv2.IMREAD_GRAYSCALE) / 255
    show(im_pattern)
    # imwrite(im_pattern, "strawberries-binary")
    kernels = [np.ones((3, 3), np.uint8),
               cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3)),
               np.ones((5, 5), np.uint8),
               cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)),
               cv2.getStructuringElement(cv2.MORPH_CROSS, (5, 5))]

    names = ['square_3', 'cross_3', 'square', 'ellipse', 'cross']

    for name, kernel in zip(names, kernels):
        closing = cv2.morphologyEx(im_pattern, cv2.MORPH_CLOSE, kernel)
        # show(closing)
        dilation = cv2.dilate(closing, kernel, iterations=2)
        # show(dilation)

        imwrite(dilation, "closing_dilation_" + name)

        dilation = cv2.dilate(im_pattern, kernel, iterations=2)
        # show(dilation)
        closing = cv2.morphologyEx(dilation, cv2.MORPH_CLOSE, kernel)
        # show(closing)

        imwrite(closing, "dilation_closing_" + name)

    best_kernel = kernels[2]
    dilation = cv2.dilate(im_pattern, best_kernel, iterations=2)
    closing = cv2.morphologyEx(dilation, cv2.MORPH_CLOSE, best_kernel)
    show(closing)

    for name, kernel in zip(names[2:-1], kernels[2:-1]):
        erosion = cv2.erode(closing, kernel, iterations=1)
        # show(erosion)

        imwrite(erosion, "erosion_" + name)

        opening = cv2.morphologyEx(erosion, cv2.MORPH_OPEN, kernel)
        # show(opening)

        imwrite(opening, "erosion_opening_" + name)

        opening = cv2.morphologyEx(closing, cv2.MORPH_OPEN, kernel)
        # show(opening)

        imwrite(opening, "opening_" + name)

        erosion = cv2.erode(opening, kernel, iterations=1)
        # show(erosion)

        imwrite(erosion, "opening_erosion_" + name)


if __name__ == '__main__':
    main()
