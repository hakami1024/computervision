### Basics and Hands-on of Computer Vision

 Repo contains work, done during the CV course, which I took in University of Helsinki in spring 2018.
 
 Full version (with all data and outputs) is in working_dir, it's too heavy to be displayed by bitbucket.
 
 Course info can be found here: https://courses.helsinki.fi/en/data20004/122409032