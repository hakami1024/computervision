import numpy as np
import cv2


def main():
    im_poster = cv2.imread("poster.jpeg", cv2.IMREAD_COLOR)
    im_frame = cv2.imread("frame.jpeg", cv2.IMREAD_COLOR)

    cv2.imshow('', im_poster)
    cv2.waitKey(0)

    cv2.imshow('', im_frame)
    cv2.waitKey(0)

    orb = cv2.ORB_create()

    kp_frame, des_frame = orb.detectAndCompute(im_frame, None)
    kp_poster, des_poster = orb.detectAndCompute(im_poster, None)

    orb_frame = np.zeros(shape=im_frame.shape)
    orb_poster = np.zeros(shape=im_poster.shape)

    orb_frame = cv2.drawKeypoints(im_frame, kp_frame, orb_frame, color=(0,255,0), flags=0)
    orb_poster = cv2.drawKeypoints(im_poster, kp_poster, orb_poster, color=(0,255,0), flags=0)

    cv2.imshow('', orb_poster)
    cv2.waitKey(0)

    cv2.imshow('', orb_frame)
    cv2.waitKey(0)

    knn = cv2.ml.KNearest_create()

    print(des_poster.shape)
    print(len(kp_poster))

    knn.train(des_poster.astype(np.float32), cv2.ml.ROW_SAMPLE, np.array(range(len(kp_poster))))
    ret, result, neighbours, dist = knn.findNearest(des_frame.astype(np.float32), k=1)

    poster_resized = np.zeros(shape=(orb_frame.shape[0], orb_poster.shape[1], 3)).astype(np.uint8)

    poster_resized[0:orb_poster.shape[0], 0:orb_poster.shape[1], 0:3] = orb_poster

    cv2.imshow('', poster_resized)
    cv2.waitKey(0)

    combo = np.hstack((poster_resized, orb_frame))

    print(poster_resized.shape)
    print(orb_frame.shape)

    width_poster = poster_resized.shape[1]

    for i, j in enumerate(result):
        i, j = int(i), int(j)
        pt_poster = [int(c) for c in kp_poster[i].pt]
        pt_frame = [int(c) for c in kp_frame[j].pt]
        cv2.line(combo, tuple(pt_poster), (width_poster+pt_frame[0], pt_frame[1]),
             color=(0, 255, 0), thickness=1)

    cv2.imshow('', combo)
    cv2.waitKey(0)


if __name__ == '__main__':
    main()