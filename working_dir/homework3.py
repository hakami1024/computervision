import gzip
import pickle

import numpy as np
from cv2 import ml


def lbp_features(im):
    result = np.zeros(im.shape)

    powers = np.array([[2**0, 2**1, 2**2],
                       [2**7,  0,   2**3],
                       [2**6, 2**5, 2**4]])

    for i in range(1, im.shape[0]-1):
        for j in range(1, im.shape[1]-1):
            result[i, j] = ((im[i-1:i+2, j-1:j+2] > im[i, j])*powers).sum()

    return result[1:-2, 1:-2]


if __name__ == '__main__':

    with gzip.open('mnist.pkl.gz', 'rb') as fs:
        train_set, valid_set, test_set = pickle.load(fs, encoding='latin1')

    train_x, train_y = train_set
    test_x,  test_y  = test_set

    train_x = train_x.reshape((train_x.shape[0], 28, 28))[0:1000]
    test_x = test_x.reshape((test_x.shape[0], 28, 28))[0:1000]

    train_y = train_y[0:1000]
    test_y = test_y[0:1000]

    lbp_train = np.array([lbp_features(im).flatten().astype(np.float32) for im in train_x])
    lbp_test = np.array([lbp_features(im).flatten().astype(np.float32) for im in test_x])

    knn = ml.KNearest_create()

    print(lbp_train.shape, train_y.shape)
    knn.train(lbp_train, ml.ROW_SAMPLE, train_y)
    ret, result, neighbours, dist = knn.findNearest(lbp_test, k=5)
    # Now we check the accuracy of classification
    # For that, compare the result with test_labels and check which are wrong
    matches = result.flatten() == test_y
    correct = np.count_nonzero(matches)
    accuracy = correct * 100 / 1000
    print(accuracy)


