import cv2
from cv2 import ximgproc
import numpy as np

def imwrite(im, name):
    cv2.imwrite(name+'.jpg', im)


masks = []
masks_direction = []

im = None
result = []

for i in range(3):
    for j in range(3) if i != 1 else [0, 2]:
        mask = np.zeros((3, 3), dtype=int)
        mask[1, 1] = 1
        mask[i, j] = 1
        masks.append(mask)

        masks_direction.append((i - 1, j - 1))


def sceleton_to_graph(img):
    global im
    im = img.copy()

    # TODO Ideally the image should be zero-padded
    last_sum = np.nan

    # im_nodes = np.zeros(im.shape)

    while np.any(im > 0):
        cur_sum = im.sum()
        print(cur_sum)
        i, j = 1, 1

        if(cur_sum == last_sum):
            imwrite(im / 255, "bear_updated")
            break
        else:
            last_sum = cur_sum

        print("new turn")

        while i < im.shape[0]-1:
            while j < im.shape[1]-1:
                im_part = im[i - 1: i + 2, j - 1: j + 2]

                if im[i, j] != 255:
                    j+=1
                    continue

                if im_part.sum() != 255 * 2:
                    j+=1
                    continue

                find_nodes(i, j)

            i += 1
            j = 1

    return np.array(result)


def find_nodes(i, j):
    im_part = im[i - 1: i + 2, j - 1: j + 2]

    if im[i, j] != 255:
        return

    # if im_part.sum() != 255 * 2:
    #     return

    for k, mask in enumerate(masks):
        if (mask * im_part).sum() == 255*2:  # node found
            print("Node found: ", i, " ", j)
            node = (i, j)
            im[i, j] = 0

            i_upd = i + masks_direction[k][0]
            j_upd = j + masks_direction[k][1]

            i_end, j_end = find_line_end(i_upd, j_upd)

            if (i_end != i_upd or j_end != j_upd):
                result.append([node, (i_end, j_end)])
                print("node ended: ", i_end, " ", j_end)

                cv2.imshow('', im)
                cv2.waitKey(0)
                return
            # else:
            #     im[i_upd, j_upd] = 0



def find_line_end(i, j):
    im_part = im[i - 1: i + 2, j - 1: j + 2]

    if(im_part.sum() != 255*2):
        find_nodes(i, j)
        # if im_part.sum() > 0:
        #     for p in [-1, 0, 1]:
        #         for q in [-1, 0, 1] if i != 0 else [-1, 1]:
        #             find_nodes(i+p, j+q)
        # im[i, j]=255
        return i, j

    for k, mask in enumerate(masks):
        if (mask * im_part).sum() == 255*2:
            im[i, j] = 0

            i += masks_direction[k][0]
            j += masks_direction[k][1]

            return find_line_end(i, j)
    return i, j


def main(im_name):
    im = cv2.imread(im_name+".pbm", cv2.IMREAD_GRAYSCALE)

    imwrite(im, im_name)
    result = cv2.distanceTransform(im, cv2.DIST_L2, 3)
    result = result/result.max()

    imwrite(result, im_name+'_distanceTransform')
    cv2.imshow('', result)
    cv2.waitKey(0)

    thinned = cv2.ximgproc.thinning(im, thinningType=cv2.ximgproc.THINNING_ZHANGSUEN)
    imwrite(thinned, im_name+"_thinned")

    thinned = cv2.ximgproc.thinning(im, thinningType=cv2.ximgproc.THINNING_GUOHALL)
    imwrite(thinned, im_name+"_thinned_guohall")

    result = sceleton_to_graph(thinned)

    # for i, node1 in enumerate(result):
    #     node2 = node1[1]
    #     node1 = node1[0]
    #
    #     for j, node3 in enumerate(result):
    #         if i != j:
    #             node4 = node3[1]
    #             node3 = node3[0]
    #             if np.sqrt(((np.array(node1) - np.array(node3))**2).sum()) < 6:
    #                 result[j, 0] = node1
    #             if np.sqrt(((np.array(node1) - np.array(node4))**2).sum()) < 6:
    #                 result[j, 1] = node1
    #             if np.sqrt(((np.array(node2) - np.array(node3))**2).sum()) < 6:
    #                 result[j, 0] = node2
    #             if np.sqrt(((np.array(node2) - np.array(node4))**2).sum()) < 6:
    #                 result[j, 1] = node2

    thinned_color = np.zeros(shape=(thinned.shape[0], thinned.shape[1], 3))

    print(thinned_color.shape)

    thinned = np.minimum(im/2 + thinned, 255)

    for i in range(thinned.shape[0]):
        for j in range(thinned.shape[1]):
            thinned_color[i, j] = [thinned[i, j], thinned[i, j], thinned[i, j]]

    for i in range(result.shape[0]):
        cv2.line(thinned_color, (result[i, 0][1], result[i, 0][0]), (result[i, 1][1], result[i, 1][0]), color=(0, 255, 0), thickness=1)
        for j in range(result.shape[1]):
            cv2.circle(thinned_color, (result[i, j][1], result[i, j][0]), 2, (0, 0, 255), -1)


    print(result)

    imwrite(thinned_color, im_name+"_thinned_graph")

    # cv2.thinning()

if __name__ == '__main__':
    main("umbrella")