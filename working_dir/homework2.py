import sys
import tkinter as tk

import numpy as np
import  cv2


def S( i0, j0, i1, j1, integral_img):
    return integral_img[i1, j1] - integral_img[i1, j0-1] - integral_img[i0-1, j1] + integral_img[i0, j0]


def integral_image(img):
    integral_img = np.pad(img.copy(), [(1, 1), (1, 1)], mode="constant")

    i = 1
    j = 1
    while i < integral_img.shape[0]-1 or j < integral_img.shape[1]-1:

        integral_img[i, j] += integral_img[i-1, j] + integral_img[i, j-1] - integral_img[i-1, j-1]
        if j+1 < integral_img.shape[1]:
            j += 1
        else:
            j = 1
            i += 1

    return integral_img


def airport():
    # for i in range(2, 12):

    root = tk.Tk()
    app = Application(master=root)
    app.mainloop()

    # (270, 590), (3700, 6900) - the best is the second
    # (180, 450), (2800, 5500) - max roads


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)

        self.img = cv2.imread("marion_airport.tiff", cv2.IMREAD_GRAYSCALE)

        self.min3 = 270
        self.max3 = 590
        self.min5 = 3700
        self.max5 = 6900
        # self.minLineLength = 100
        # self.maxLineGap = 10

        self.ro = 1
        self.theta = np.pi/180

        self.threshold = 20

        self.pack()
        # self.create_widgets()
        self.create_hough_widgets()
        self.redr()

    def redr(self):
        print("(", self.min3, ", ", self.max3, "), (", self.min5, ", ", self.max5, ")")
        # print("\t(", self.minLineLength, ", ", self.maxLineGap, ")")
        print("\t(", self.ro, ", ", self.theta, ", ", self.threshold, ")")

        edges3 = cv2.Canny(self.img, self.min3, self.max3, apertureSize=3, L2gradient=False)
        cv2.imshow('3', edges3)
        edges5 = cv2.Canny(self.img, self.min5, self.max5, apertureSize=5, L2gradient=False)
        cv2.imshow('5', edges5)

        im = self.img.copy()
        im = np.dstack([im]*3).astype(np.uint8)
        # lines = cv2.HoughLinesP(edges3, 1, np.pi / 180, 100, self.minLineLength, self.maxLineGap)
        # for x1, y1, x2, y2 in lines[0]:
        lines = cv2.HoughLines(edges3, self.ro, self.theta, self.threshold)
        for rho, theta in lines[:, 0]:
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a * rho
            y0 = b * rho
            x1 = int(x0 + 1000 * (-b))
            y1 = int(y0 + 1000 * (a))
            x2 = int(x0 - 1000 * (-b))
            y2 = int(y0 - 1000 * (a))

            cv2.line(im, (x1, y1), (x2, y2), (0, 255, 0), 2)
        cv2.imshow('hough3', im)

        im = self.img.copy()
        im = np.dstack([im]*3).astype(np.uint8)

        lines = cv2.HoughLines(edges5, self.ro, self.theta, self.threshold)
        for rho, theta in lines[:, 0]:
            # rho, theta = line[0]
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a * rho
            y0 = b * rho
            x1 = int(x0 + 1000 * (-b))
            y1 = int(y0 + 1000 * (a))
            x2 = int(x0 - 1000 * (-b))
            y2 = int(y0 - 1000 * (a))

            cv2.line(im, (x1, y1), (x2, y2), (0, 255, 0), 2)
        cv2.imshow('hough5', im)

        cv2.waitKey(15)

    def create_hough_widgets(self):
        self.inc_min = tk.Button(self)
        self.inc_min["text"] = "threshold+10"
        self.inc_min["command"] = self.inc_thresh
        self.inc_min.pack()

        self.inc_max = tk.Button(self)
        self.inc_max["text"] = "theta+10"
        self.inc_max["command"] = self.inc_theta
        self.inc_max.pack()

        self.inc_max = tk.Button(self)
        self.inc_max["text"] = "ro+10"
        self.inc_max["command"] = self.inc_ro
        self.inc_max.pack()

        self.dec_min = tk.Button(self)
        self.dec_min["text"] = "threshold-10"
        self.dec_min["command"] = self.dec_thresh
        self.dec_min.pack()

        self.dec_max = tk.Button(self)
        self.dec_max["text"] = "theta-10"
        self.dec_max["command"] = self.dec_theta
        self.dec_max.pack()

        self.dec_max = tk.Button(self)
        self.dec_max["text"] = "ro-10"
        self.dec_max["command"] = self.dec_ro
        self.dec_max.pack()

    def create_widgets(self):
        self.inc_min = tk.Button(self)
        self.inc_min["text"] = "min+10"
        self.inc_min["command"] = self.inc_minimum
        self.inc_min.pack()

        self.inc_max = tk.Button(self)
        self.inc_max["text"] = "max+10"
        self.inc_max["command"] = self.inc_maximum
        self.inc_max.pack()

        self.dec_min = tk.Button(self)
        self.dec_min["text"] = "min-10"
        self.dec_min["command"] = self.dec_minimum
        self.dec_min.pack()

        self.dec_max = tk.Button(self)
        self.dec_max["text"] = "max-10"
        self.dec_max["command"] = self.dec_maximum
        self.dec_max.pack()

    def inc_minimum(self):
        self.min3 +=10
        self.min5 += 100

        self.redr()

    def inc_maximum(self):
        self.max3 += 10
        self.max5 += 100
        self.redr()

    def dec_minimum(self):
        self.min3 -= 10
        self.min5 -=100
        self.redr()

    def dec_maximum(self):
        self.max3 -= 10
        self.max5 -= 100
        self.redr()

    def inc_min_len(self):
        self.minLineLength +=10

        self.redr()

    def inc_max_gap(self):
        self.maxLineGap += 2
        self.redr()

    def dec_min_len(self):
        self.minLineLength -= 10
        self.redr()

    def dec_max_gap(self):
        self.maxLineGap -= 2
        self.redr()

    def inc_thresh(self):
        self.threshold +=10

        self.redr()

    def inc_theta(self):
        self.theta *= 2
        self.redr()

    def dec_theta(self):
        self.theta /= 2
        self.redr()

    def dec_thresh(self):
        self.threshold -= 10
        self.redr()

    def inc_ro(self):
        self.ro += 2
        self.redr()

    def dec_ro(self):
        self.ro -= 2
        self.redr()




def main():
    # strawberries()
    airport()


def strawberries():
    img = cv2.imread("strawberries-binary.pbm", cv2.IMREAD_GRAYSCALE) / 255
    int_img = integral_image(img)
    max_i, max_j, max = 0, 0, 0
    for i in range(100, img.shape[0]):
        for j in range(100, img.shape[1]):
            s = S(i - 100, j - 100, i, j, int_img)
            if s > max:
                max = s
                max_i = i
                max_j = j
    im_col = np.dstack([img * 255] * 3).astype(np.uint8)
    # im_col[max_i-100:max_i, max_j-100:max_j] = (0, 0, 255)
    im = cv2.imread('strawberries.tiff')
    cv2.imshow('', im)
    cv2.waitKey(0)
    cv2.rectangle(im, (max_j - 100, max_i - 100), (max_j, max_i), (255, 0, 0))
    cv2.imshow('', im)
    cv2.waitKey(0)
    cv2.imwrite('strawberries_rect.jpg', im)


if __name__ == '__main__':
    main()