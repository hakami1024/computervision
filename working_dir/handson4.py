import time

import cv2
import numpy as np


def match(train_feature, query_feature):

    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=50)  # or pass empty dictionary

    flann = cv2.FlannBasedMatcher(index_params, search_params)
    res=flann.knnMatch(query_feature.astype(np.float32), train_feature.astype(np.float32), k=1)

    return res


def get_links_flann(kp_train, kp_test, des_train, des_test):

    result = np.array(list([[m[0].trainIdx, m[0].queryIdx] for m in match(des_train, des_test)]))
    return indices_to_coords(kp_train, kp_test, result), result


def get_links(kp_train, kp_test, des_train, des_test):
    knn = cv2.ml.KNearest_create()

    knn.train(des_train.astype(np.float32), cv2.ml.ROW_SAMPLE, np.array([i for i in range(len(kp_train))]))
    ret, result, neighbours, dist = knn.findNearest(des_test.astype(np.float32), k=1)

    return indices_to_coords(kp_train, kp_test, enumerate(result)), result


def indices_to_coords(kp_train, kp_test, result):
    coord_pairs = []
    for i, j in result:
        pt_train = kp_train[i].pt
        pt_test = kp_test[int(j)].pt

        coord_pairs.append([pt_train, pt_test])
    return np.array(coord_pairs, dtype=int)


def get_orb_features(im1, im2):
    orb = cv2.ORB_create()

    kp1, des1 = orb.detectAndCompute(im1, None)
    kp2, des2 = orb.detectAndCompute(im2, None)

    orb1 = np.zeros(shape=im1.shape)
    orb2 = np.zeros(shape=im2.shape)

    orb1 = cv2.drawKeypoints(im1, kp1, orb1, color=(0, 255, 0), flags=0)
    orb2 = cv2.drawKeypoints(im2, kp2, orb2, color=(0, 255, 0), flags=0)

    # cv2.imshow('', orb1)
    # cv2.waitKey(0)
    # cv2.imshow('', orb2)
    # cv2.waitKey(0)

    return kp1, kp2, des1, des2, orb1, orb2


def visualise_links(coord_pairs, orb_train, orb_test, silent = False):

    train_height = orb_train.shape[0]
    max_height = max(orb_test.shape[0], train_height)

    im_max = orb_train if max_height == train_height else orb_test
    im_min = orb_test if max_height == train_height else orb_train

    min_width = im_min.shape[1]

    min_resized = np.zeros(shape=(max_height, min_width, 3), dtype=np.uint8)
    min_resized[0:im_min.shape[0], 0:im_min.shape[1], 0:im_min.shape[2]] = im_min

    if not silent:
        cv2.imshow('', min_resized)
        cv2.waitKey(0)

    combo = np.hstack((min_resized, im_max))

    for coords in coord_pairs:
        if max_height != train_height: #if min=train is drawn first
            pt_1, pt_2 = coords
        else:                           #if min=test is drawn first
            pt_2, pt_1 = coords
        cv2.line(combo, tuple(pt_1), (min_width+pt_2[0], pt_2[1]),
                 color=(0, 255, 0), thickness=1)

    if not silent:
        cv2.imshow('', combo)
        cv2.waitKey(0)

    return combo


def frame_task(kp_train, kp_test, des_train, des_test, orb_train, orb_test, silent = True, flann=False):

    if flann:
        coord_pairs, coord_pairs_inds = get_links_flann(kp_train, kp_test, des_train, des_test)
    else:
        coord_pairs, coord_pairs_inds = get_links(kp_train, kp_test, des_train, des_test)

    combo = visualise_links(coord_pairs, orb_train, orb_test, silent)

    return coord_pairs, combo, coord_pairs_inds


def main():
    im_poster = cv2.imread("poster.jpeg", cv2.IMREAD_COLOR)
    im_frame = cv2.imread("frame.jpeg", cv2.IMREAD_COLOR)

    cv2.imshow('', im_poster)
    cv2.waitKey(0)

    cv2.imshow('', im_frame)
    cv2.waitKey(0)

    kp_poster, kp_frame, des_poster, des_frame, orb_poster, orb_frame = get_orb_features(im_poster, im_frame)

    cv2.imwrite("orb_poster.jpg", orb_poster)
    cv2.imwrite("orb_frame.jpg", orb_frame)

    coords1, combo1, coord_inds_1 = frame_task(kp_poster, kp_frame, des_poster, des_frame, orb_poster, orb_frame)
    coords2, combo2, coord_inds_2 = frame_task(kp_frame, kp_poster, des_frame, des_poster, orb_frame, orb_poster)

    cv2.imwrite("combo1.jpg", combo1)
    cv2.imwrite("combo2.jpg", combo2)

    coords2[:, [0, 1]] = coords2[:, [1, 0]]
    coord_inds_1 = np.array(list(enumerate(coord_inds_1)), dtype=int)
    coord_inds_2 = np.array(list(enumerate(coord_inds_2)), dtype=int)

    np.set_printoptions(threshold=np.nan )
    coord_inds_2[:, [0, 1]] = coord_inds_2[:, [1, 0]]

    coord_inds_cum = np.array([x for x in (set(tuple(x) for x in coord_inds_1) & set(tuple(x) for x in coord_inds_2))])

    coords_cum = indices_to_coords(kp_poster, kp_frame, coord_inds_cum)

    combo_cum = visualise_links(np.array(coords_cum), orb_poster, orb_frame)
    cv2.imwrite("combo_cum.jpg", combo_cum)


def main_video():
    im_poster = cv2.imread("poster.jpeg", cv2.IMREAD_COLOR)
    cap = cv2.VideoCapture('video.avi')

    video = None
    if not cap.isOpened():
        print("Error")

    time_sum = 0
    time_count = 0

    while (cap.isOpened()):
        # Capture frame-by-frame
        ret, im_frame = cap.read()
        if ret:
            if video is None:
                video = cv2.VideoWriter('video_out.avi', cv2.VideoWriter_fourcc('M','J','P','G'),
                                        10, (im_poster.shape[1]+im_frame.shape[1], im_frame.shape[0]))

            start = time.time()
            kp_poster, kp_frame, des_poster, des_frame, orb_poster, orb_frame = get_orb_features(im_poster, im_frame)

            coords1, combo1, coord_inds_1 = frame_task(kp_poster, kp_frame, des_poster, des_frame, orb_poster,
                                                       orb_frame, flann=True)
            coords2, combo2, coord_inds_2 = frame_task(kp_frame, kp_poster, des_frame, des_poster, orb_frame,
                                                       orb_poster, flann=True)

            coords2[:, [0, 1]] = coords2[:, [1, 0]]

            if coord_inds_1.shape[1] == 1:
                coord_inds_1 = np.array(list(enumerate(coord_inds_1)), dtype=int)
                coord_inds_2 = np.array(list(enumerate(coord_inds_2)), dtype=int)

            np.set_printoptions(threshold=np.nan)
            coord_inds_2[:, [0, 1]] = coord_inds_2[:, [1, 0]]

            coord_inds_cum = np.array(
                [x for x in (set(tuple(x) for x in coord_inds_1) & set(tuple(x) for x in coord_inds_2))])

            coords_cum = indices_to_coords(kp_poster, kp_frame, coord_inds_cum)

            combo = visualise_links(np.array(coords_cum), orb_poster, orb_frame, silent=True)
            end = time.time()

            time_sum += end - start
            time_count += 1
            video.write(combo)
        else:
            break

    print("Average processing time: ", time_sum/time_count)
    # When everything done, release the video capture object
    cap.release()
    video.release()

if __name__ == '__main__':
    # main()
    main_video()