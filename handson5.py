import cv2
import numpy as np


def im_to_stereo(imgL, imgR):
    print(imgL.shape)
    stereo = cv2.StereoSGBM_create(numDisparities=16, blockSize=15, mode=cv2.StereoSGBM_MODE_HH)
    # frame1_new=cv2.cvtColor(imgL, cv2.COLOR_BGR2GRAY)
    # frame2_new=cv2.cvtColor(imgR, cv2.COLOR_BGR2GRAY)
    disparity = np.zeros_like(imgL)
    for i in range(3):
        disparity[:, :, i] = stereo.compute(imgL[:, :, i], imgR[:, :, i])
    # plt.imshow(disparity, 'gray')
    # plt.show()

    return disparity


def main():

    i = 0
    video = None

    while i < 1000:
        path = gen_path(i)
        imgL = cv2.imread(path, cv2.IMREAD_COLOR)
        imgR = cv2.imread(gen_path(i+1), cv2.IMREAD_COLOR)

        disp = im_to_stereo(imgL, imgR)
        # da = np.array(disp, dtype=np.float32)
        # da /= np.max(da)
        # da = np.dstack([da] * 3)
        disp[disp[:, :, 0] < 0] = (0, 0, 255)
        disp[disp[:, :, 1] < 0] = (0, 0, 255)
        disp[disp[:, :, 2] < 0] = (0, 0, 255)

        combo = np.hstack((imgL, imgR, disp.astype(np.uint8)))

        if video is None:
            video = cv2.VideoWriter('disp_out_sg_col.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 10,
                                    (combo.shape[1], combo.shape[0]))

        video.write(combo)
        cv2.imshow('', disp)

        i+=2

    video.release()


def gen_path(i):
    dir = "frames/"
    s = str(i)
    return dir + s.zfill(8) + ".jpeg"


frame_idx = 0
def next_frame():
    global frame_idx
    f = '{:08d}'.format(frame_idx)
    print(f)
    frame_idx +=2
    return cv2.imread('frames/'+f+'.jpeg')

def optical_flow():
    # params for ShiTomasi corner detection
    feature_params = dict(maxCorners=100,
                          qualityLevel=0.3,
                          minDistance=7,
                          blockSize=7)
    # Parameters for lucas kanade optical flow
    lk_params = dict(winSize=(15, 15),
                     maxLevel=20,
                     criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))
    # Create some random colors
    color = np.random.randint(0, 255, (100, 3))
    # Take first frame and find corners in it
    old_frame = next_frame()
    old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
    p0 = cv2.goodFeaturesToTrack(old_gray, mask=None, **feature_params)
    # Create a mask image for drawing purposes
    mask = np.zeros_like(old_frame)

    video = None

    while True:
        frame = next_frame()
        if frame is None:
            break
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # calculate optical flow
        p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None, **lk_params)
        # p1, st, err = cv2.calcOpticalFlowFarneback(old_gray, frame_gray, p0, **farn_params)#, p0, None, **lk_params)
        # Select good points
        if p1 is None:
            p0 = cv2.goodFeaturesToTrack(old_gray, mask=None, **feature_params)
            p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None, **lk_params)
            # p1, st, err = cv2.calcOpticalFlowFarneback(old_gray, frame_gray, p0, None, **lk_params)

            mask = np.zeros_like(old_frame)

        good_new = p1[st == 1]
        good_old = p0[st == 1]
        # draw the tracks
        for i, (new, old) in enumerate(zip(good_new, good_old)):
            a, b = new.ravel()
            c, d = old.ravel()
            mask = cv2.line(mask, (a, b), (c, d), color[i].tolist(), 2)
            frame = cv2.circle(frame, (a, b), 5, color[i].tolist(), -1)
        img = cv2.add(frame, mask)
        cv2.imshow('frame', img)
        if video is None:
            video = cv2.VideoWriter('movements.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 10,
                                    (img.shape[1], img.shape[0]))
        video.write(img)

        k = cv2.waitKey(30) & 0xff
        if k == 27:
            break
        # Now update the previous frame and previous points
        p0 = good_new.reshape(-1, 1, 2)
        old_gray = frame_gray.copy()
    video.release()


def draw_flow(img, flow, step=16):
    h, w = img.shape[:2]
    y, x = np.mgrid[step/2:h:step, step/2:w:step].reshape(2,-1).astype(int)
    fx, fy = flow[y,x].T
    lines = np.vstack([x, y, x+fx, y+fy]).T.reshape(-1, 2, 2)
    lines = np.int32(lines + 0.5)
    vis = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    cv2.polylines(vis, lines, 0, (0, 255, 0))
    for (x1, y1), (x2, y2) in lines:
        cv2.circle(vis, (x1, y1), 1, (0, 255, 0), -1)
    return vis


def draw_hsv(flow):
    h, w = flow.shape[:2]
    fx, fy = flow[:,:,0], flow[:,:,1]
    ang = np.arctan2(fy, fx) + np.pi
    v = np.sqrt(fx*fx+fy*fy)
    hsv = np.zeros((h, w, 3), np.uint8)
    hsv[...,0] = ang*(180/np.pi/2)
    hsv[...,1] = 255
    hsv[...,2] = np.minimum(v*4, 255)
    bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    return bgr


def warp_flow(img, flow):
    h, w = flow.shape[:2]
    flow = -flow
    flow[:,:,0] += np.arange(w)
    flow[:,:,1] += np.arange(h)[:,np.newaxis]
    res = cv2.remap(img, flow, None, cv2.INTER_LINEAR)
    return res


def optical_flow_farn():
    prev = next_frame()
    prevgray = cv2.cvtColor(prev, cv2.COLOR_BGR2GRAY)
    show_hsv = True
    show_glitch = True
    cur_glitch = prev.copy()

    flow = None
    for i in range(499):
        img = next_frame()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        flow = cv2.calcOpticalFlowFarneback(prevgray, gray, flow, pyr_scale=0.5, levels=3, winsize=15,
                                            iterations=10, poly_n=15, poly_sigma=3.2, flags=cv2.OPTFLOW_FARNEBACK_GAUSSIAN)
        prevgray = gray

        cv2.imshow('flow', draw_flow(gray, flow))
        if show_hsv:
            cv2.imshow('flow HSV', draw_hsv(flow))
        # if show_glitch:
        #     cur_glitch = img.copy()
        #     cur_glitch = warp_flow(cur_glitch, flow)
        #     cv2.imshow('glitch', cur_glitch)

        k = cv2.waitKey(30) & 0xff
        if k == 27:
            break


if __name__ == '__main__':
    # main()
    optical_flow()
    # optical_flow_farn()