import cv2
import numpy as np


def main():
    im_people = cv2.imread("people.jpeg", cv2.IMREAD_COLOR)

    dir = '/usr/share/opencv/haarcascades/'
    face_cascade = cv2.CascadeClassifier(dir+'haarcascade_frontalface_default.xml')
    eye_cascade = cv2.CascadeClassifier(dir+'haarcascade_eye.xml')
    gray = cv2.cvtColor(im_people, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray)
    for (x, y, w, h) in faces:
        cv2.rectangle(im_people, (x, y), (x + w, y + h), (255, 0, 0), 2)
        roi_gray = gray[y:y + h, x:x + w]
        roi_color = im_people[y:y + h, x:x + w]
        eyes = eye_cascade.detectMultiScale(gray, scaleFactor=10.05, minNeighbors=0)
        for (ex, ey, ew, eh) in eyes:
            cv2.rectangle(im_people, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)
    cv2.imshow('img', im_people)
    cv2.imwrite('people_rec_strange.jpg', im_people)
    cv2.waitKey(0)

    im_people = cv2.imread("people.jpeg", cv2.IMREAD_COLOR)

    face_cascade = cv2.CascadeClassifier(dir+'haarcascade_frontalface_default.xml')
    eye_cascade = cv2.CascadeClassifier(dir+'haarcascade_eye_tree_eyeglasses.xml')
    gray = cv2.cvtColor(im_people, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray, 1.05, 10)
    for (x, y, w, h) in faces:
        cv2.rectangle(im_people, (x, y), (x + w, y + h), (255, 0, 0), 2)
        roi_gray = gray[y:y + h, x:x + w]
        roi_color = im_people[y:y + h, x:x + w]
        eyes = eye_cascade.detectMultiScale(roi_gray, 1.01, minNeighbors=1)
        for (ex, ey, ew, eh) in eyes:
            cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)
    cv2.imshow('img', im_people)
    cv2.imwrite('people_rec_eyeglasses.jpg', im_people)
    cv2.waitKey(0)


if __name__ == '__main__':
    main()