import cv2
import numpy as np
import matplotlib.pyplot as plt


def main():
    im = cv2.imread("strawberries.tiff")

    point_red = np.array([0, 0, 255])#im[131, 198]
    point_green = np.array([0, 255, 0]) #im[172, 168]

    print(point_green)
    print(point_red)

    points_count_red = [0]*445
    points_sum_red = []

    max_val = im.shape[0] * im.shape[1]
    print(max_val)
    
    for i in range(im.shape[0]):
        for j in range(im.shape[1]):
            r = int(np.sqrt(((im[i, j]-point_red)**2).sum()))
            points_count_red[r] += 1

    for i in range(256):
        sum_val = sum(points_count_red[0:i])
        points_sum_red.append(sum_val)

        if sum_val == max_val:
            break
        
    plt.plot(points_sum_red)
    plt.show()

    points_count_green = [0] * 445
    points_sum_green = []

    for i in range(im.shape[0]):
        for j in range(im.shape[1]):
            r = int(np.sqrt(((im[i, j] - point_green) ** 2).sum()))
            points_count_green[r] += 1

    for i in range(256):
        sum_val = sum(points_count_green[0:i])
        points_sum_green.append(sum_val)
        if sum_val == max_val:
            break

    plt.plot(points_sum_green)
    plt.show()

    for r in [100, 115, 170, 190]:
        im_masked = im.copy()

        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                if(((im[i, j]-point_red)**2).sum() > r**2 ):
                    im_masked[i, j] = [0, 0, 0]

        cv2.imwrite("im_red_"+str(r)+".jpg", im_masked)
        cv2.imshow('', im_masked)
        cv2.waitKey(0)

    for r in [170, 190, 200, 225]:
        im_masked = im.copy()

        for i in range(im.shape[0]):
            for j in range(im.shape[1]):
                if(((im[i, j]-point_green)**2).sum() > r**2 ):
                    im_masked[i, j] = [0, 0, 0]

        cv2.imwrite("im_green_"+str(r)+".jpg", im_masked)
        cv2.imshow('', im_masked)
        cv2.waitKey(0)

    r = 180
    im_masked = im.copy()

    for i in range(im.shape[0]):
        for j in range(im.shape[1]):
            if (((im[i, j]-point_red)**2).sum() > r**2 ) or\
                        (((im[i, j] - point_green) ** 2).sum() > r ** 2):
                im_masked[i, j] = [0, 0, 0]

    cv2.imwrite("im_final_" + str(r) + ".jpg", im_masked)
    cv2.imshow('', im_masked)
    cv2.waitKey(0)


if __name__ == '__main__':
    main()