import cv2
import numpy as np
import matplotlib.pyplot as plt


def random_gauss(A, r):
    return A + np.random.normal(scale = r, size=A.shape)


def random_saltpepper(A, r):
    return np.clip(A + np.random.binomial(1, 0.5*r, size=A.shape)*np.random.choice([1, -1], A.shape), 0, 1)


def clip(A):
    return np.clip(A, 0, 1)


def lowpass(A, n):
    filter = np.ones((n, n))/(n**2)

    result = np.zeros(A.shape)

    A_padded = np.pad(A, n-1, 'reflect')

    for i in range(A.shape[0]):
        for j in range(A.shape[1]):
                result[i, j] = (A_padded[i:(i+n), j:(j+n)]*filter).sum()

    return result


def highpass(A, n):
    large_num = 2
    filter = -large_num*np.ones((n, n))/(n**2-1)

    filter[int(n/2), int(n/2)] = large_num

    result = np.zeros(A.shape)

    A_padded = np.pad(A, n - 1, 'reflect')

    for i in range(A.shape[0]):
        for j in range(A.shape[1]):
            result[i, j] = (A_padded[i:(i + n), j:(j + n)] * filter).sum()

    return result


def median(A, n):
    result = np.zeros(A.shape)

    A_padded = np.pad(A, n - 1, 'reflect')

    for i in range(A.shape[0]):
        for j in range(A.shape[1]):
            result[i, j] = np.median(A_padded[i:(i + n), j:(j + n)])

    return result


def imwrite(im, name):
    cv2.imwrite(name+'.jpg', cv2.resize(im*255, None, fx=20, fy=20,
                                     interpolation=cv2.INTER_AREA))


def show(im):
    cv2.imshow('', cv2.resize(im, None, fx=20, fy=20,
                                     interpolation=cv2.INTER_AREA))

    cv2.waitKey(0)


def main():
    im_pattern = cv2.imread("pattern.pbm", cv2.IMREAD_GRAYSCALE)/255
    im_messi = cv2.imread("messi-gray.tiff", cv2.IMREAD_GRAYSCALE)/255

    imwrite(im_pattern, "pattern")
    imwrite(im_messi, "messi")

    gauss_05 = ex_part1(im_messi, im_pattern, 0.05)
    gauss_15 = ex_part1(im_messi, im_pattern, 0.15)

    impuls_1 = ex_part2(im_messi, im_pattern, 0.1)
    impuls_5 = ex_part2(im_messi, im_pattern, 0.5)

    for n in [3, 5]:
        for i, im, name in zip(range(10), [im_pattern, im_messi,
                             gauss_05[0], gauss_05[1], gauss_15[0], gauss_15[1],
                            impuls_1[0], impuls_1[1], impuls_5[0], impuls_5[1]],
                            ["pattern", "messi",
                             "gauss_pattern_05", "gauss_messi_05", "gauss_pattern_15", "gauss_messi_15",
                             "impuls_pattern_1", "impuls_messi_1","impuls_pattern_5", "impuls_messi_5"]):

            ex_part3(im, name, n)#, view=(i%2 == 0))

    for n in [3, 5]:
        for i, im, name in zip(range(6), [im_pattern, im_messi,
                             gauss_05[1], gauss_15[1],
                             impuls_1[1], impuls_5[1]],
                            ["pattern", "messi",
                             "gauss_messi_05", "gauss_messi_15",
                             "impuls_messi_1", "impuls_messi_5"]):

            ex_part4(im, name, n)#, view=(i%2 == 0))

    for n in [3, 5]:
        for i, im, name in zip(range(10), [im_pattern, im_messi,
                             gauss_05[0], gauss_05[1], gauss_15[0], gauss_15[1],
                            impuls_1[0], impuls_1[1], impuls_5[0], impuls_5[1]],
                            ["pattern", "messi",
                             "gauss_pattern_05", "gauss_messi_05", "gauss_pattern_15", "gauss_messi_15",
                             "impuls_pattern_1", "impuls_messi_1","impuls_pattern_5", "impuls_messi_5"]):

            ex_part5(im, name, n)#, view=(i%2 == 0))

    ex_part6()


def ex_part6():
    im_messi = cv2.imread("messi-gray.tiff", cv2.IMREAD_GRAYSCALE)

    h = [0] * 256
    for i in im_messi.flatten():
        h[i] += 1

    plt.plot(h)
    plt.savefig('hist_original_messi.png')
    plt.show()

    equ = cv2.equalizeHist(im_messi)
    cv2.imwrite("messi_equalized.png", equ)

    h = [0] * 256
    for i in equ.flatten():
        h[i] += 1

    plt.plot(h)
    plt.savefig('hist_equalized_messi.png')
    plt.show()


def ex_part5(im, name, n, view=False):
    im_median = median(im, n)
    if view:
        show(im_median)
    imwrite(im_median, "median_"+name+"_"+str(n))


def ex_part4(im, name, n, view=False):
    im_highpass = clip(highpass(im, n)+0.5)
    if view:
        show(im_highpass)
    imwrite(im_highpass, "highpass_"+name+"_"+str(n))


def ex_part3(im, name, n, view = False):
    im_lowpass = clip(lowpass(im, n))
    if view:
        show(im_lowpass)
    imwrite(im_lowpass, "lowpass_" + name + "_" + str(n))


def ex_part2(im_messi, im_pattern, r):

    im_pattern_impuls = random_saltpepper(im_pattern, r)
    im_messi_impuls = random_saltpepper(im_messi, r)

    show(im_pattern_impuls)

    imwrite(im_pattern_impuls, "impuls_pattern_"+str(r))
    imwrite(im_messi_impuls, "impuls_messi_"+str(r))

    return im_pattern_impuls, im_messi_impuls


def ex_part1(im_messi, im_pattern, sigma):

    im_pattern_gauss = clip(random_gauss(im_pattern, sigma))
    im_messi_gauss = clip(random_gauss(im_messi, sigma))

    show(im_pattern_gauss)

    imwrite(im_pattern_gauss, "gauss_pattern_"+str(sigma))
    imwrite(im_messi_gauss, "gauss_messi_"+str(sigma))

    return im_pattern_gauss, im_messi_gauss


if __name__ == '__main__':
    main()
